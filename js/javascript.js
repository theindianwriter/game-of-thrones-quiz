obj = {
        question : ["Q1. Who was Ned stark\'s bastard son?","Q2. What was Arya Stark\'s direwolf name?","Q3. What is Tyrion\'s nick name?","Q4. Who is the leader of White Walker\'s ?","Q5. Who is he ?"],
        correct_answer : ["4. None of These","1. Nymeria","1. The Imp","2. Night\'s King","3. Berric Dandarion"],
}

// obj = {
//    1: {
//       text: 'ques text',
//       options: {
//          1: 'opt 1',
//          2: 'opt 2',
//       },
//       correct_ans: 2,
//    },
//    2: 'ques2',
//    1: 'ques1',
//    1: 'ques1',
//    1: 'ques1',
// }






page = 0;
function dynamic_page(){
   switch(page){
         case 0:
           let next0 = '<div class = "question_box"><p>Please enter your details to start the quiz</p> ';
           next0 += '<form>';
           next0 +='Name: <input id = "fullname" type="text" name="fullname" value =""><br>';
           next0 +='Email: <input id = "email" type="text" name="email" value =""><br>';
           next0 +='<input class ="submit_button" type="button" value ="submit" onclick ="submit_function()"></form></div>';
           next0 += '<div class = "box2"><img src="assets/pictures/morghulis.jpg"></div>'
           document.getElementById('dynamic_div').innerHTML = next0;
           page++;
           break;

         case 1: 
            let next1 = '<div class = "question_box"> ';
            next1 += '<p>Q1. Who was Ned stark\'s bastard son?</p>';
            next1 += '<button class = "question_button" id = "no1"onclick ="option1()">1. Arya Stark</button><br>';
            next1 += '<button class = "question_button" id = "no2" onclick ="option2()">2. Jon Snow</button><br>';
            next1 += '<button class = "question_button" id = "no3" onclick ="option3()">3. Bran Stark</button><br>';
            next1 += '<button class = "question_button" id = "no4" onclick ="option4()">4. None of These</button><br>';
            next1 += '</div>';
            next1 += '<div class = "box2"><img src="assets/pictures/ned.jpg"></div>'
            next1 += '<button class = "next_button"  onclick ="next_question()">NEXT QUESTION</button>';
            document.getElementById('dynamic_div').innerHTML = next1;
            page++;
            break;
        case 2:
           next2 = '<div class = "question_box"> ';
           next2 += '<p>Q2. What was Arya Stark\'s direwolf name?</p>';
           next2 += '<button class = "question_button" id = "no1" onclick ="option1()">1. Nymeria</button><br>';
           next2 += '<button class = "question_button" id = "no2" onclick ="option2()">2. Lady</button><br>';
           next2 += '<button class = "question_button" id = "no3" onclick ="option3()">3. Ghost</button><br>';
           next2 += '<button class = "question_button" id = "no4" onclick ="option4()">4. None of These</button><br>';
           next2 += '</div>';
           next2 += '<div class = "box2"><img src="assets/pictures/aryastark.jpg"></div>'
           next2 += '<button class = "next_button"  onclick ="next_question()">NEXT QUESTION</button>';
           document.getElementById('dynamic_div').innerHTML = next2;
           page++;
           break;
        case 3:
           let next3 = '<div class = "question_box"> ';
           next3 += '<p>Q3. What is Tyrion\'s nick name?</p>';
           next3 += '<button class = "question_button" id = "no1" onclick ="option1()">1. The Imp</button><br>';
           next3 += '<button class = "question_button" id = "no2" onclick ="option2()">2. Hound</button><br>';
           next3 += '<button class = "question_button" id = "no3" onclick ="option3()">3. Mountain</button><br>';
           next3 += '<button class = "question_button" id = "no4" onclick ="option4()">4. None of These</button><br>';
           next3 += '</div>';
           next3 += '<div class = "box2"><img src="assets/pictures/tyrion.jpg"></div>'
           next3 += '<button class = "next_button"  onclick ="next_question()">NEXT QUESTION</button>';
           document.getElementById('dynamic_div').innerHTML = next3;
           page++;
           break;
         case 4:
            let next4 = '<div class = "question_box"> ';
            next4 += '<p>Q4. Who is the leader of White Walker\'s ?</p>';
            next4 += '<button class = "question_button" id = "no1" onclick ="option1()">1. Walker King</button><br>';
            next4 += '<button class = "question_button" id = "no2" onclick ="option2()">2. Night\'s King</button><br>';
            next4 += '<button class = "question_button" id = "no3" onclick ="option3()">3. Onion King</button><br>';
            next4 += '<button class = "question_button" id = "no4" onclick ="option4()">4. None of These</button><br>';
            next4 += '</div>';
            next4 += '<div class = "box2"><img src="assets/pictures/whitewalker.jpg"></div>'
            next4 += '<button class = "next_button"  onclick ="next_question()">NEXT QUESTION</button>';
            document.getElementById('dynamic_div').innerHTML = next4;
            page++;
            break;
        case 5:
          let next5 = '<div class = "question_box"> ';
          next5 += '<p>Q5. Who is he ?</p>';
          next5 += '<button class = "question_button" id = "no1" onclick ="option1()">1. Martell</button><br>';
          next5 += '<button class = "question_button" id = "no2" onclick ="option2()">2. Ilyrio</button><br>';
          next5 += '<button class = "question_button" id = "no3" onclick ="option3()">3. Berric Dandarion</button><br>';
          next5 += '<button class = "question_button" id = "no4" onclick ="option4()">4. None of These</button><br>';
          next5 += '</div>';
          next5 += '<div class = "box2"><img src="assets/pictures/bericdandarion.jpg"></div>'
          next5 += '<button class = "next_button"  onclick ="next_question()">FINISH</button>';
          document.getElementById('dynamic_div').innerHTML = next5;
          page++;
          break;
        default:
           let  last = '<div class = "lastPage" id = "last_page">';
           last += '<h2>Hi!! '+naam+' Your Answers are</h2>';
           last += '<p>';
           for(j = 0 ; j < 5; j++){
              last += obj.question[j]+'<br>';
              last += '  '+'correct answer: '+obj.correct_answer[j]+ '<br>';
               if(val[j] == "")
               {
                   last += ' '+'Your answer: '+'no answer <span class = "wrong">NOT ATTEMPTED!!</span><br><br><br>';
               }
               else {
                   if(val[j] == obj.correct_answer[j])
                      last += '  '+'Your answer: '+val[j]+'<span class = "right"> RIGHT </span><br><br>';
                   else
                   last += '  '+'Your answer: '+val[j]+' <span class ="wrong">WRONG!! </span > <br><br>';  
               }
               
           }last +='</p></div>';
           document.getElementById('dynamic_div').innerHTML = last;
          
    }   
}